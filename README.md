# Customer Segmentation – K-Means Clustering

•	The mall customer dataset contains information about customers like 'CustomerID', 'Gender', ‘Age’, ‘Annual Income’ and their ‘Spending Score’. The data contains no NULL values or missing values. The variable 'Gender' is coded into numerical type.

•	Now, since we implemented K-Means technique which is a distance-based algorithm, it was important to normalize, standardize, or to choose any other option in which the distance has some comparable meaning for all the columns. We used MinMaxScaler tool for it. Also, we dropped the 'CustomerID' variable, because it was not useful in the clustering process.

•	It was observed that there were more female than male records in the data. Interestingly, it was found that distribution of variables like ‘Age’, ‘Annual Income’ and ‘Spending Score’ and scatter plots among them were very identical across two genders and that of merged data (Male + Female). That is why the variable ‘Gender’ was not considered in the process of creating the segments, because it was unlikely to create any statistically significant clusters of customers based on their gender.

•	In order to find the optimum n_clusters in the K-Means algorithm, two metrics were implemented; **Inertia** and **Silhouette score**. The minimum Inertia and Silhouette Score close to 1 and -1 was desired. 

•	For X1 = 'Age and Spending Score', the optimum n_clusters chosen was 3.

•	For X2 = 'Age and Annual Income', the optimum n_clusters chosen was 3.

•	For X3 = 'Annual Income and Spending Score', the optimum n_clusters chosen was 5.

•	For X4 = 'Age and Annual Income and Spending Score', the optimum n_clusters chosen was 6.

•	The clusters were visualized for all sets of variables stated above.

***Techniques Used:*** KMeans, plotly, pyplot, silhouette_score, seaborn, sklearn. (**Python**)

